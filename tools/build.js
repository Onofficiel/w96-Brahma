/*
Windows 96 Userland Source Code.
Copyright (C) 2023 Windows96.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
Tool to build and minify all apps.

Currently used for all system apps.
Note to developers: If your app needs full kernel access, place your app script in the kernel apps folder instead.
*/

const fsUtil = require('./lib/util/fs');
const terser = require('terser');
const path = require('path');
const fs = require('fs');
const confmt = require('./lib/util/confmt');
const { die_error } = require('./lib/util/common');
const { BINSPEC_DEFAULT } = require('./lib/build/binspec');
const buildSpecCfg = require('../config/build_spec.json');
const compat = require('../config/compatibility.json');
const babel = require("@babel/core");

// Give process title
process.title = "win96sys-ul-build";

// Console formatting functions
const BUILD_STEP = (m)=>console.log(confmt.format(`$[magenta]${m}$[reset]`));
const BUILD_WARN = (m)=>console.log(confmt.format(`$[yellow]${m}$[reset]`));
const BUILD_ERR = (m)=>console.log(confmt.format(`$[red]${m}$[reset]`));

/**
 * Build script entry point.
 */
async function main() {
    console.log(confmt.format("$[cyan]Windows 96 System Userland Build Tool\nCopyright (C) SYS36 2023.\n$[reset]"));

    const IS_DEV = process.argv.includes("--dev") || false;
    if (IS_DEV) BUILD_WARN("Building in dev mode!");

    BUILD_STEP("Preparing build environment...");

    if(!(await fsUtil.existsAsync("../src")))
        die_error("No source directory found (are you in the correct working directory?) ** STOP **");

    // Create build directory if it does not exist
    if(!(await fsUtil.existsAsync("../output")))
        await fs.promises.mkdir("../output");

    // Compile apps

    BUILD_STEP("Compiling apps...\n");

    // Calculate number of items to build and create list of apps to compile.
    let totalAppsToBuild = 0;
    let appsToBuild = [];

    for(let searchPath of buildSpecCfg.searchPaths) {
        if(!(await fsUtil.existsAsync(`../src/${searchPath}`)))
            die_error(`Search path '${searchPath}' cannot be found (are you in the correct working directory?) ** STOP **`);

        for(let appDir of await fs.promises.readdir(`../src/${searchPath}`)) {
            const fullPath = path.resolve(`../src/${searchPath}/${appDir}`);

            if(!(await fs.promises.stat(fullPath)).isDirectory())
                continue; // Ignore non-directories

            if(!(await fsUtil.existsAsync(path.join(fullPath, "binspec.json")))) {
                // There is a bug.
                //BUILD_WARN(`Warning: Skipping ${searchPath}/${appDir} due to missing binspec!`);
                continue;
            }

            totalAppsToBuild++;

            appsToBuild.push({
                name: appDir,
                path: fullPath,
                adir: searchPath
            });
        }
    }

    // Build the apps (duh)
    let buildCounter = 0;

    for(let app of appsToBuild) {
        let buildNo = ++buildCounter;

        try {
            console.log(`[${buildNo} / ${totalAppsToBuild}] BUILD ${app.name}`);

            // Read binspec
            /** @type {BINSPEC_DEFAULT} */
            const binSpec = {
                ...BINSPEC_DEFAULT,
                ...JSON.parse(await fs.promises.readFile(path.join(app.path, "binspec.json"),
                {
                    encoding: "utf-8"
                }))
            }

            // Decide whether binspec is good enough for build
            if(!binSpec.name)
                throw "BinSpec does not specify a target name."

            if(binSpec.build.jsIn.length == 0)
                throw "No target can be built, since this application does not specify any input JavaScript files."

            // Do the building
            // Step 1: Combine all JS files into one
            // There are no import directives in the JS source itself.
            // It is up to the developer to arrange the source files properly in their BinSpec.

            // Compute BinSpec String
            // Used for program metadata
            const BSTR = JSON.stringify({
                icn: binSpec.icon,
                cpr: binSpec.copyright,
                dsc: binSpec.description,
                frn: binSpec.friendlyName,
                aut: binSpec.aut,
                ver: binSpec.version,
                ssy: binSpec.subsys
            });

            // Win96 reads a max of 256 bytes to acquire meta info for a given bin.
            if(BSTR.length > 242)
                throw "Total WRT string > 256";

            // BSTR cannot contain newlines
            if(BSTR.includes("\n"))
                throw "Illegal character in WRT string: \\n";

            const WRT_HEADER = `//!wrt${binSpec.stripBsp ? "" : ` $BSPEC:${BSTR}`}`;

            // Bundle and minfiy scripts

            let totalScript = `${WRT_HEADER}\n\n${binSpec.shiftArgv ? `this.boxedEnv.args.shift();\n` : ""}`;
            let tempScript = "";

            for(let scriptName of binSpec.build.jsIn) {
                const scriptPath = path.join(app.path, scriptName);

                if(!(await fsUtil.existsAsync(scriptPath)))
                    throw `Cannot find script: ${scriptPath}`

                // Add script to temp script (one big script which will be minified)
                tempScript += `${await fs.promises.readFile(scriptPath, {
                    encoding: "utf-8"
                })}\n\n`;
            }

            // Run temp script through babel
            if(buildSpecCfg.useBabel) {
                tempScript = babel.transformSync(tempScript, {
                    presets: [["@babel/preset-env", {modules: false}]],
                    ...(compat.useCompatibilityPreset ? compat.presets.find(x => x.name == compat.currentPreset).babelProps[1] : {}),
                    parserOpts: {
                        allowAwaitOutsideFunction: true,
                        allowReturnOutsideFunction: true
                    }
                }).code;
            }

            // Minify total script
            totalScript += (!IS_DEV ? (await terser.minify(tempScript, {
                parse: {
                    bare_returns: true
                },
                keep_classnames: true
            })).code : tempScript) + "\n";

            if(binSpec.startupClass)
                totalScript += `\nreturn await WApplication.execAsync(new ${binSpec.startupClass}(), this.boxedEnv.args, this);`;


            // Create paths
            const outPath = path.resolve(`../output/${app.adir}/${binSpec.name}`);
            const outDir = path.dirname(outPath);
            await fs.promises.mkdir(outDir, {recursive: true})

            // Write the binary
            // Perform a check if the bin is actually changed
            // This is needed for DevConnector
            if(await fsUtil.existsAsync(outPath)) {
                const oldText = await fs.promises.readFile(outPath, { encoding: 'utf-8' });

                if(oldText != totalScript) {
                    await fs.promises.writeFile(outPath, totalScript, {
                        encoding: 'utf-8'
                    });
                }
            } else {
                // Just write
                await fs.promises.writeFile(outPath, totalScript, {
                    encoding: 'utf-8'
                });
            }

            // Script building Complete!
            // TODO minify and compile CSS and HTML files
        } catch(e) {
            // Log build errors
            BUILD_ERR(`[${buildNo} / ${totalAppsToBuild}] FAIL ${app.name}: ${e}`);
        }
    }
}

main();
