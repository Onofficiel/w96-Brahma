/*
Windows 96 Userland Source Code.
Copyright (C) 2023 Windows96.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const { BINSPEC_DEFAULT } = require('./binspec');
const terser = require('terser');
const fsUtil = require('../util/fs');
const path = require('path');
const fs = require('fs');

async function buildApp(app) {
    // Read binspec
    /** @type {BINSPEC_DEFAULT} */
    const binSpec = {
        ...BINSPEC_DEFAULT,
        ...JSON.parse(await fs.promises.readFile(path.join(app.path, "binspec.json"),
            {
                encoding: "utf-8"
            }))
    }

    // Decide whether binspec is good enough for build
    if (!binSpec.name)
        throw "BinSpec does not specify a target name."

    if (binSpec.build.jsIn.length == 0)
        throw "No target can be built, since this application does not specify any input JavaScript files."

    // Do the building
    // Step 1: Combine all JS files into one
    // There are no import directives in the JS source itself.
    // It is up to the developer to arrange the source files properly in their BinSpec.

    // Compute BinSpec String
    // Used for program metadata
    const BSTR = JSON.stringify({
        icn: binSpec.icon,
        cpr: binSpec.copyright,
        dsc: binSpec.description,
        frn: binSpec.friendlyName,
        aut: binSpec.aut,
        ver: binSpec.version
    });

    // Win96 reads a max of 256 bytes to acquire meta info for a given bin.
    if (BSTR.length > 242)
        throw "Total WRT string > 256";

    // BSTR cannot contain newlines
    if (BSTR.includes("\n"))
        throw "Illegal character in WRT string: \\n";

    const WRT_HEADER = `//!wrt${binSpec.stripBsp ? "" : ` $BSPEC:${BSTR}`}`;

    // Bundle and minfiy scripts

    let totalScript = `${WRT_HEADER}\n\n${binSpec.shiftArgv ? `this.boxedEnv.args.shift();\n` : ""}`;
    let tempScript = "";

    for (let scriptName of binSpec.build.jsIn) {
        const scriptPath = path.join(app.path, scriptName);

        if (!(await fsUtil.existsAsync(scriptPath)))
            throw `Cannot find script: ${scriptPath}`

        // Add script to temp script (one big script which will be minified)
        tempScript += `${await fs.promises.readFile(scriptPath, {
            encoding: "utf-8"
        })}\n\n`;
    }

    // Minify total script

    totalScript += (await terser.minify(tempScript, {
        parse: {
            bare_returns: true
        },
        keep_classnames: true
    })).code + "\n";

    if (binSpec.startupClass)
        totalScript += `\nreturn await WApplication.execAsync(new ${binSpec.startupClass}(), this.boxedEnv.args);`;

    // Write the binary
    const outPath = path.resolve(`../output/${binSpec.name}`);

    await fs.promises.writeFile(outPath, totalScript, {
        encoding: 'utf-8'
    });

    // Script building Complete!
    // TODO minify and compile CSS and HTML files
}

module.exports = {
    buildApp
}
