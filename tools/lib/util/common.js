/*
Windows 96 Userland Source Code.
Copyright (C) 2023 Windows96.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const confmt = require('./confmt');

/**
 * Exits the process with the specified message.
 * @param {String} message The message to print.
 * @param {Number} code The error code to use.
 * @returns {never}
 */
function die_error(message, code = 1) {
    console.error(confmt.format(`$[red]${message}$[reset]`));
    process.exit(code);
}

module.exports = {
    die_error
}