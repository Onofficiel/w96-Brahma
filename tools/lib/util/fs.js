/*
Windows 96 Userland Source Code.
Copyright (C) 2023 Windows96.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const fs = require('fs');
const path = require('path');

/**
 * Checks if the specified item exists.
 * @param {String} path The path of the item to check.
 * @returns 
 */
function existsAsync(path) {
    return new Promise((resolve)=>{
        fs.promises.access(path, fs.constants.F_OK)
            .then(()=>resolve(true))
            .catch(()=>resolve(false));
    });
}

/**
 * Walks the specified path.
 * @param {string} dir The path to walk.
 * @param {Function} filter Search filter.
 */
async function walkAsync(dir, filter = null) {
    let paths = [];
    const contents = await fs.promises.readdir(dir);
    
    for(let p of contents) {
        const fullPath = path.join(dir, p);

        if(filter != null)
            if(!filter(fullPath))
                continue;

        // Walk dir
        if((await fs.promises.stat(fullPath)).isDirectory())
            paths = [...paths, ...await walkAsync(fullPath, filter)];
        else
            paths.push(fullPath);
    }

    return paths;
}

module.exports = {
    existsAsync,
    walkAsync
}