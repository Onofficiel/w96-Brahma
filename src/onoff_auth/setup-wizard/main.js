const { WizardBaseApplication } = w96.app.templates;
const { RadioBox, CheckBox } = w96.ui.components;
const { p3 } = w96.net;
const { KConsole } = w96.ktypes;
const { cprovider } = w96.sec;

class OnoffAccountSetup extends WizardBaseApplication {
	constructor() {
		super();
	}

	async main(argv) {
		await super.main(argv);

		// Window Config
		this.appWindow.setTitle("Onofficiel Account");

		// Intro page
		const introPage = this.findPage("intro");
		introPage.titleText = "Connect to your Onofficiel account";
		introPage.graphic =
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKMAAAEuCAYAAADiNaIyAAAAAXNSR0IArs4c6QAAHMJJREFUeF7tXc2a2zYS1LzAJpc4VzsX+/2fJr7EZztPkNPsx9FAA0EgUF1dDYIU57IbC+if6kKjCFLUy2+/f3799f3HJf19+vrlcv73iccWfHj57+fr642J5/85EdgQgZOMG4J/ur5HYBIyLs35BauNYShm8Bw1CwIvp2YENPLfP25r5ciaelnnf254zTBJZ5xlbZ5xbInAScYb+oP3f9IdOW1LjsG+TzJejlxemAdTDDw1o0gjJUofWVMujI3M7+yMU/SEM4gFgUOR8dxw903qQ5Fxq2uRfVOAj169+E/NKNKMW9zLjdZwo+1P0xnVq0xtj+8f50wUgWnIiAasHXdSVounz9qTk9EHnnT2uS4uq5rxPDe7Ui3yXO20f4/v2Rml7e3R2ELmo//lD2N7cpWR8dxl7svwDCQsieclpYyMnhVxmLnZijzJaK/qsc4ZXy+XT9+A5xMHfOdHQUbZbrM8tzzgyyVLZ/Ro7LMz2hcwNKMko3cLg5wOHqTOcb9klLWNmAqqC3WNcq6k1Tnul4wxHJJZVRdKFpjQkDrHY2nGic4F1wrl0VSznUvWcvTkt5POONf2hDQXdddAfI4eo85xJ2QcDbPfn7pQ/oj0FtQ5nmTU1+jNorpQQWG6zKpzfCLNeN3qPZrGotl2oRmd57JPqhldC3iTyequsUkSHafqHM9tOqjKbKHYS7XaHZ/aHRFlumyOazGcZFRWJ7OlLlQtTOstR/VdIHWOT6QZr+U8ima0EnHJ3XvvuMTv1IxBnUxtVt018vgYIpb5KbpkP0eb6Jhom7YFriaP2l6/UJxHVBumHaBFXC8h1TlOREauOJGzPMtDXaja2eXySstfy+v6jFe9afh0ZDzfzxjz/KP6nNHaERPh8g6ZNGNORo+GPjVjrw1M8rm6MyrsrRGahUwRU+773KbZShi3RuWW6LGlJOTcZPSIrCBSbGVWWSglgWrakyW3MsclrvOcMehdO0rN2DqWYTSfyt6Tacai1e6o8yq7htJWdGf0lOjUjEH7uJJASlvRZPTAeZLRg15jrpJASltTkzGdM5bnUeU51R7+e+vfMUmFLs/zln9PFwmMxqudD3rqMa1mXMhYW+DsFVZQo9md2Ydulv2wkTUZ5dW0sssqbd2upnvgRD8X1/O/x8/VhVIR0m/n4xJFnePb0Y6l2GfHxNBSF8pPosfv5eTyAcvqfpQ6RzMZ83DUz8flmsujiWb4vWzlOWOZT8LGgv8amVkNu3Yh5LHnImMOCrOylHM851vKOPJFVC5cr58aoVo1sI63xjdVZ2xdlVkTK8f7yOWbTcfeeCWeSt60CIbGHRWL166kM653gI1IgVYlcJy6a+ShegjpJUwrDq9tORnLbcOjIfasISM1Y+0cE1lXFo2JaHb5vWnr1TSStForWX3OMH7WzqjU+eocb0/teFp/r/je9t2zP+Pn6kKtXRgt/97qeJEXMeocq/emI4j5bIRUFypJFuaica2e3pqoc4SeZ1R1HrVmmVlTqjUjfk741+XX939uJUuaPf/ffKv2aHq5Zvzv5yt8B0bRMb2rUbUwou2ou4bC3hqhWSwWe/l5ibe25kfIaivMmow3aKu/LcYryLN2jOLBT0lIdY5mMraEtKXoHkAtfrYaqyyUkkA17cnWQpnjEhekGXv3etmCH1lDKjVj66KF0Xwqe8Gakbtj4tGS7KpkF8CoecquobQ1dWe0XMC0CskR8vrTTUckpJpAoxaRxY86R1ozrgXNkfLjsXwLGDOP9RSK259EaBice3KsRdvRjK+XT1+Xc6uPlwshGoWF5UgaUqkZrfiPGi/XjPm9aeV2+ewdUt012AUeOW9pVPkvZHr5U31qp9ahrEl5ziPtSRn2Fmsi5PjnIOP9D7vb63YPbvcRMq+DZ+2QJxntq7hLxsWkV4PYw7rOiNKQP7//WN6z+faHaGAm/1Mz2vGFyJiTie2Uyg4536b8uNzOzmhvQWYylp0SdTlWQ6JRxY07yWjHliLjIyHxXqXskPZ0lxl4rJz9j+1JsaN4Yoieq15wNBk9mo4FKUpDMpow5bCmOU/NOEAzlkSaQUOy5I6cp+4akbGyttU5ujpjSoIh5NE1JCtHWGLMMI/hQR73271pBXBsIKxv1t+oorF5jYovwo+3JtV702ygrKYb7S9CI7Y0JJuff971qagRfwlTz7mt/NuB7OpgOwnrb0SBFh9sXqPiU/hR1WD1EbIFRPYQhAnu6BpSUfRhNtjCOwPsPs/IrmyGkJ5Owvpz4nc3vVbDjerqT2uDwKHvwLCZnRoy7t53S6OO1sQqf93OmCfNkJLtWKM7MpPbOUeLQDgZl3AZQs6vITfYx7S1n84aTMazQ05Xu8MFBGnGUhOwKJwa8tSQrXNIc2c8OyS7FM95PQSGk/G4GrIH9fl5DwGajGeH7EE71+d7uNwCNePyvr/rt0Zqez4L+6khGxrynT2ee71r9VKdC6rtuzsj2iFz4pUAM2Q+zyEZ1OaeIyWjmnAt6EaeQ+5hi5ubZlh0EjJuWayzQ2KF3sMoUDPa3rUzWpOwQLOadXR+4/21rxGi4pF0RpYMynlnh7xHc8vdiq3rociYOp0VDObeudXHOb6PwGHIiF7Vr0FyErJPlugRh9CMW987l2qob18uv/6eRKO/Xi6flniM7+dkx4d2xi11y6kho/uY3n4oGfXhtizeU3/kOeTYPI/rbSgZc4LUjlUiYD47ZASqMTbDNCPTmWp3cBT3Zlno5jqHvHZ+BR6spltwjPQv7YxsF2qRRXWVy8Sm8s0uhinnBV4IuMnIFJkF2UMOtlM3Yw0sDIvRbPMsELnIOJKICWQPIdM2YymY15/FV9xYCyXiouhZpjRjz+ioz1lNZ41vVo0VreFG2zd1xi06YY84TOdi8mD89GI/P79HACYjU8BRYDNEYfJh/IzC4Ah+IDIyhRsNDkMUS16M/dEY7N0fpBn3kmS0hmTtn5oTu9fe7IyWztEj7NqBdm+e6fOXy91DBshca4576pD7uIb+qFLz/YxIMVtjLLf8mHPANd8WwkBkzF4Aa7Htxe/Z5svfXLsAqCgYRJJGtSwxWHxZ7D4bmbz5TvFO756mYpO0aDyWkJH3akef823t76EzWoqSkySyY4yI6d5H+8XskbmyC+8I8yRkHFEchpCWuCz2LXZ5kuzt8oPPNM28I6OlIMnAr+8fX2v0h9O3YI3RQhyLbYvdflaTjNiY/zfNaCnEBxF/hD7ftqZhrKXradLyc8R+mnNqRt13ZG6d0UrGLTtDZKyo7S3zRxbLHsdQZJyhEChprEdNUXb3SI7RMb+RMa4AsSIkKm7U7gyLcjRhVv0JSm36VVXLuV0KeoSmQguCxq+2Z9WsTzn+9XIxdcYZO0FEF4uwiRJ8j+PyW7mWW8BlrubOOBtYKHEs2rFms7YLzbg41fVRPDOAEhQm48zAo4REc0DtWQiuJskIexYcrPHUagGTMQFv1YB/fP1y+Tf4XS0oEKhmTON6di32RmpoRnMqOmAPr/LzEj+YjGhXsQakGI+uYDQH1N7eO6MlT0WdWjaW2kBkRIsYHXDLPgKsJQ+1vS2xWXxv0fmsOUNk3LIDIMdXCHESMAgh1fasRUFyRmxa8kDsRY+ByMhokNEaCQHeovHU9qLxGNH5cvzK45wyPyYeiIxbdsbWakwdBCHOnjoj2oEseaM218YhO8raXJSYEBk9gXhBQOcjhbHkobaH5uEtqNdPajzlqYnCbk+7QmSctTPmWwMKFkJIhIiWTovG1iKi18Zt/spD7AguphgA4VvibCaj9Zxx1HiEQHvSjEg+JnJkg2vaL1rTtuynz8xkZAGInIcWDl39qD31jmHxa8UTzd1qVzV+yR0moxp4VRJJhyD2LAVBiGGx14sP8dezkUuHKM1XxrB2VY3GmndMkIwvl+t3Xeb8QwuJkkdtz6sFAfl1c4Hm6Kkkis9dAwOSAMl4Dd2iuUZqEBRYNH61vZ/Lra53o+gxBxIDmo/inDgREODUW+j5OFSjns8zVqqOrny2C6H2a4RkfSLkro3xxGqN/2nIeLdlNCpjAZ8hhsV+rgFZMrHzLHGi3bKXj4mMaEFZAFrzaglbALMQB7VrsbnkhtrdEmdrnJ5al/hR35tWaBCvpmQLi5x7ogBbNBsSr8WeFb+1q97rv9//vvRj/u3XvaB4lePKfNe/qtrxb+0KbMBWHeP9egBCGmvnQmwq8Vy7SOr5QOJcq2MiFlPnFBf1vene3s8E9DYHEB9twOorqFeEPF6kIFvaW8MWuUpvxY3kbak7Ek9pT/CunY9X5NIkNEy0gBbRwawF6aVmIXZpy4KFgohsrEici23J+xlHaEjLSssJg2hE6xaD5IsWwBKfNc7awqn5e1gwxQaD5NvTsAgeklfiWTtQr1t4OkA+17qSc8BaiqFnFwG+Z8MqHVqYerqiJc5eXXu4yMgYRcheAmwRPIRHClTGXd6RQArHdsLSNktGJM9eHnefv14un759WZ0if6e3MoGjEJHp1p7cUX89H8pa9jv9S10zKlckqolyzWFabZXBVo2D+kPPAdfseeczcSL4e+O14r3Gr2G/A1O7qOitTBR8tBPU7FliQDpFy15vviUWy1a8hqMnVqY2ve7Y/bk2D0DegK3ze8WOJmMPK6t2W7uIqnVYK1ZbEnGJtea/S8a1idbko8dHE3GJv+fDU+D1uffnLKsxADcMep0p372i61XjFfTbgTYNGXMfswWOSrMgPtY0GEpE6JxvJRBUcyJ4eOJF7OfXAL3xaSzUGXPDqhVjXMirbnvdyrs1o90CLS4TD5Njr06eeHu20c/LGExkTK3V1inR0PBxyX/+BDU6u6franYQMrDFReJB/KP5I40lwt9afHn+ZjKiusMKDjreCxRS/DwWxF8UERHfKG7lODZm1h+yI8CasaUBegEqt2Tk3AzVKL24LRoNLa5FM9b8q9536Ym3h6/185tm/O33z6+qFWjtOj0yLJ9vGRvqGy3sFN3pvTOwMSM1Q8c8aMZExnLFogZrAOcrmiGo4hzNUvhlLPtQLltUdh5bFwse6CL0xFI9Z1zImIzCQZD7biJ8+b+epHpzmcVg7cgMsSxzSLh70DS/kwNzoetlfUC1M5bDrXv+jONzwlvxsuazZr+lOVEyqjRiTfMjMSg0eg3Pbme0XkXiRY5a2/UI2G5o7Yh5gWuRtLoLQgQcX27kVjG8PUZ2248/Ym++UWJEq+ZgxFu/xT6TL1tQdp4lH2RsLY7UOhg8ej5beXdfb9IPaGzXayUb1xHbOa75ZTpjH+9euW2f9zBTx+MiY7l1R2mImqZZ/g311wO1VSKrRizHR2pGNH8Pfj36evF5wGvl8YVuZ2xd3PSSWP/89fJ6ebm9DIm14yFg8qlY+crOyOpWFsO04K/z1x9y8eKE1MpMxgRWuWI9YFjmeq6SIxYWqv3KjR6dZ8HGMxYhC3P+i9hNcVNkrCXtXTk9IC1J9Wypuw9DLGYOkpdnjAVjpN5WexAZ0UuU8s5L65ytp3EeOqDwMUm5BlphgOKccYRmRDVwj+gp3964tR0KIqPV+FrnzAn2GLiQbY2AkRVtzZftcuw8a3zW8ZaOZrXdkkrUW8i8AWw1P4KIeYe3ypde0SPj7dWgF1tvPvJ5mZ/7XTuI063HRBeV7XC9gkfH3atLL77e/LXP1/KSvGuHDSp6nkez9jRt+TlKyJoGbBVtC82o0pC1LbmVj/yNEikA6KInUCaO7iooGWvE63Wg0bmsLY5enK3mguTQ/dqBJ4DoztcSw6N9e8i4xIrgjBS0RaTytCONZezWLkZrpymWOnTJ2BPoFmeRYxlAZfH0np5+uVx+/d1+jyVCxiVeJs+ebcamDLvMEPUdmIhAGJsjNaFFQzIasEeYms3Wua7XniVfxbntEi/cGRmtwxCsN2eWVczg0YudIVAPL+vnvRit9nrj85xdZMwdtQ+0eyHdf+7VHjZv2tEjtKM24kdrIwhZw0lGRgSgnuhFbOxhjJeQS45bdcloIrbyojSjSyN8+3In5mc4R3Pl8/Y7Kh8XJwiJrP6iFmBXc7+/adYab208gsvQzhgF6lC7wAEqArylAyH2UAwsflGb5Tg23pOMLOKdeUhBLMTIJQ4acq3zdecCi23tWqFruzPgJKMXwcZ8NSFrHWiLiz0kLwbWeM34BBqxp6l6henNn+FzpjP38k6fp/zOzogi5hiHdBLLlu0IBZqKEM+4m1f9ljmvktFzbti6MwChccBBCCHLTjESBoSA3nh6Gnaz5xl7gXkT18x/X/+KNmA8O4zslCmdhwUS8BSVpc7DvnaAkKN77mX4HnXyN+M55hsZgMIr8diq81nwn4qMOWFDOoOowyELqzXGsmWXdiydxuPHmqOpXit1mJaMJTHLFVZ9o+IkZEMKqSRKIijiVzHGsiAs/nZBxlpCppVoQWTgWCUho8MegfduyfgB/svl1/d/bu/ksWiUxcYs42ciZusgPRKvA5Cx6AnvFwYjVrK6G/UIGalCZsBrv2QErkbzc7tHzammksbep69/3Tq9xuKjlSjN5413v2QkM5+hA1hD73VMi72Z8i87/dORsXeVHqmJlBq1docMufM1c35PTca7jpJpzb1s6ZaOuIexJxkbVZpVW+2BWGWMyMWXmYzIZb9S46DAI8mitlrjJJprVLCKhAfauCNjnOZYzgGXq8Qfm33RKAJT5b3jFN/Mmk6peWv53sgoWfFExe1d1HCmQ8TjnbIVjt64Y+bbtoBpHq4d8VRJDODtc7zzYghHnSCjje14KI8j7V3T423M3LNzruMc9B2Yf95/xkF37/dZOudTa8b/fr5WfsVtTJfwejlS5zw75sX34icvmZTzj9o5lRjNbovQjLOndI1vWNcMvrh/po4ZpBnv3z2TA7qVJjpC55z9XHPRe39W3j2E1v+wnbHVv4d1zcBN5Igd8ynJmHNkzx1zbkLajwBXyWg3FdgGBpveY+ecm5j3BVzjFq0Ze8/THe1e6x4IOrum7N3b7m7TqiLsaeX2GrEKk54f9vO9Yv1AxkgN1Xr8jAV+63mReLG57fU5zDcybrnS97qKa0SZlZgsqUfPMz9cGxng3jVProlmIiZ6ztfTdNGfT0XGGtGP1jl1i9l262cPOE5Pxrx4e9VCs2zpsxNyV2TcvHMGH76O0O4zE3L3ZOx1zq3uhXvPWaOJmUg5Ez6HImNJTJ0+28bSKEJuk92j18OS8SjEHHFVPsvWfWgyzgKyqvNEdcpZcNonGTunGns5V2PP7VTkru0em2nI18vlcGScZZVHEKa0qe6UW2NnIiNyb3mEximLcqTzRyuJlYSskdF+mmWfkXLuklGxWpSA1YhoLeBM4/nSfWShxFdRbxbfKhlHaC4FgEe6l604l0x4sGRI80bUv5bvAxm3WBkMMbeI01vkEfMZLDe/s/UewO1J7/Iq6u1zxR6CVCDzg2jOkUSsPdG+llJKA9HWCCzMGAQ/1O5InJeYuk96o4FHjstX+wiAVN1ly46jyiEa77zf0d+B8Wqc2eYrO8oDCVfORUdpXm+jGKUhd9EZvWD25qu6SM9P6/OoDqTILSq2Eo+nJaOiSB7yrW3hVe3ucKTo+Lsm46jrHrZGMxKxzEVNAG/Od/EEFfgpNeN94W2P73cXgNCcUlN6ybjkrYynds3wFNu0ohBdEgYNUHZILw7KWGpwHZ6M3gIEccxkVnnv3YtHJCH3S0ZAt3iBNzFmwGAVETy4qGKodsbffv/8OuocaQlg1PNyHsAH8IpyodRsED6N89GIc+L9dsZGOSGgKTrMMUnRnbwYKWI4/DmjF+Q56IZF4SGEByeP31Zmh+qMHoCx8tePN8ota0Qc6agFjbs2DouzvldHEPIw54wYsLbSRWs04BqsGbAqPhsqH6Or/r99ufz6m3un+247Y17INhG5U+iIlZ8u4Njil/PyR9Y8NtmFrMZot2TMt0ZPIWpz1SDzWySWmTfek4wYzt1RLJClYW9Bu4FWBqhi9+pHTxxK3A6hGa915rbjspCjzkEjOrtHQ6oI6cFv19u0B8BEBuXKZrpjPseXz3UxYvk8XjqxvjF/GDK7JSMLXg4LDaT3MrhRG29e1pxSKh6/Vp9r6e+SjB7gZuyIZXFu+RHKw0MMFlePzzz3XWpGrOmvj9rDvXiWGAoNbMVXhefTdUbVKrYWzDreQ0bP1TXrV4Hr7sjIguUpkJVIivGePD1ShPU7hIyBWp2qGQvW3si4xLtVroxfCRn3+Dwjw+IEluccLBFEpZFQe0y+afEx+Y72l/Dc1TbNrFjPlsUWRT2PzZvtVg/+gKt61tfd1fTyc21q8Nbt8Zs+W5CZt2cLGkz+HoKM9rfUaTedkQFnZiJaGgCbO5s/689D/jcy7kUzsgApzt1Ga8SaPyb/0feqPf7OzmhpTxOMZQlpDZ3xw3bhDTWjFZaP8QxA3m2Dj1Y/k8nfc/HG+PPivQvNyACjWKl6Stkt4k+0122zBHnEHLikfn8Fij3L64xDa0avhlkA+jinu9KCObdTa05rsRn/TAPw4n12RmtlNxzPEITdIUb6SpDugoxLT/rz6xczDdgtyuxo4ASGJAwOo/zs7gLmHph47TKQWyZXDEF21Rn3cM5oqlg22Kth7jXj1fCpGderUcP7j69fLv9+x75HvYttmukIzNbEkn7kvJFYjPT1djU99t40VzYGFHZ74iIcN4vBglmYo/zsXDNihWcKgFnebhRDEHZRjvR1u5p+ds1o0TSnZmwvRK9G923TlmegHA2FWaVvLx9aLrwP9MfgIOmM2AHGG9KeHenhDsystWMK4QHm2XHYAu+7X1WdtXgMMN5V2iPjoE3hLoyROIz0dacZS+CZe5nJRtQ5XBuc9RdaRsUTnW/NvoIgKB6ML7dmXC5gal0gvEsaWgsDTMopPI9eCxV+zuDQy3+tDBG+elA8/Pj5GjHLFdUzrP58C3DYHAzrrO2i+B1uNp4eIWt2t8AbIuMmnbNwyoATrRtZcsDzBGQcRUQF1jQZc0BzrYBqEqvmYslYghQVnzUf63gmf4+GG+1vwUNCxlGdkwFIsWLhThY0cHTeo/0l2MLIuNY5PfUaA5JM8XlSvc1lc2YX4eKPRYCRBDlIQ8hYEpOt0ujCsHE+zmPLu837dhicvUQM3aZbhfSeY7Ik8Wgoq8ZTjh+dL+Mv1dSjyYd3xpQou5KYVavqzEyRPHM8ufrwNdyMfk+Q9bfpNu0lxrVAdrC8fj2kYub6ichJA8avgoiCbVpACgIzBrA9kXGr/Fi/k5CxWPNGbno03D1wRsfZo04ejbNkHzHf0klra3m0Jvf6S/PfnmdkV4QFtNrYBcj8yzoWe4qYVSvaEndr7NY5sf5VON4erk0rnA2IKYgniXqc9g65xH0fB6EbmOSLOQrc9XhiiXn83l3AtL6QtQaQslyeRBQF9F7dY+Vqj1LlsQWWHp8lKvC7diI7p1dzKAiROmSEBuzldx+/r7uz8TMLwqP5U855vNR3YCKI6VlhDJAtAntiQRaGOt5HqYFE8TGGjaeOE79vUmSspfpAUMMCVxSfBXQUKSMWsEJieHBT1A3WjLb1tbLSQFJ6E/OAiuTpiS8yNk9cS96e2Ly+ac1Y2+NTMj1NlGuLVvIqDYKQyzOmzAfNz+OzNndLvFLNWY0q04xeUHuE9Nj3rHSP39FzFV3Jg5XC/0Nn3PpdO6WW8iYZqc10hAO1y4pDL0azbc8pTdkFTA03/rrq3hpjx7PqdaTTW9qaiN4r9xYiL//7/fMr+v48i0b0akzlfD0ltrG4pUbMM0auEZj6hXZGecmYFum8YpTnQBpUdETv9hzZFRfb/wcYczmUhAtwqwAAAABJRU5ErkJggg==";
		introPage.body =
			"This wizard will help you to configure your Onofficiel account. <br> You can choose to connect to an account or create one.";
		introPage.buttons[1].onclick = () => this.openPage("choose-config", []);

		let radioActSelect = "register";

		// Config page
		this.pages.push({
			id: "choose-config",
			type: "page",

			graphic: "/system/resource/app/backup/graphic-top.png",
			titleText: "Connection or registration",
			description: "Do you already have an account ?",

			body: `
        Select an action:
        <div class="w96-radiobox"></div>
      `,
			buttons: [
				{
					class: "back",
					onclick: () => this.openPage("intro", []),
					enabled: true,
					text: "Back",
				},
				{
					class: "next",
					onclick: () => {
						this.openPage(radioActSelect, []);
					},
					enabled: true,
					text: "Next",
				},
				{
					class: "cancel",
					onclick: () => this.quit(),
					enabled: true,
					text: "Cancel",
				},
			],
			onopen: (page, args) => {
				const opts = new RadioBox([
					{
						label: "Register a new account.",
						id: "register",
						onselect: () => {
							radioActSelect = "register";
						},
						selected: radioActSelect === "register",
					},
					{
						label: "Connect to an exisitng account.",
						id: "connect",
						onselect: () => {
							radioActSelect = "connect";
						},
						selected: radioActSelect === "connect",
					},
				]);

				page.querySelector(".w96-radiobox").replaceWith(opts.getElement());
			},
		});

		let id = null;
		let password = null;

		// Register page
		this.pages.push({
			id: "register",
			type: "page",

			graphic: "/system/resource/app/backup/graphic-top.png",
			titleText: "Creating a new account",
			description: "Please fill fields bellow",

			body: `
        Fill the fields:<br>
        <br>
        <div>Identifier</div>
        <input class="w96-textbox" type="text" id="id" name="id"><br>

        <div>Password</div>
        <input class="w96-textbox" type="password" id="password" name="password"><br>

        <div>Confirm password</div>
        <input class="w96-textbox" type="password" name="password">
        
        <br>
        <div style="display: none; color: red" class="passwd-error">
          Passwords must match
        </div>
      `,
			buttons: [
				{
					class: "back",
					onclick: () => this.openPage("choose-config", []),
					enabled: true,
					text: "Back",
				},
				{
					class: "next",
					onclick: () => {
						this.openPage("server-connect", {
							type: "register",
							id,
							password,
						});
					},
					enabled: false,
					text: "Next",
				},
				{
					class: "cancel",
					onclick: () => this.quit(),
					enabled: true,
					text: "Cancel",
				},
			],
			onopen: (page, args) => {
				let passwordsMatch = false;

				page.querySelectorAll("input").forEach((el) => {
					el.style.width = "100%";
					el.style.marginBottom = "6px";
				});

				const passwdEls = Array.from(
					page.querySelectorAll("input[type='password']"),
				);
				const handle = () => {
					id = page.querySelector("#id").value;
					password = page.querySelector("#password").value;

					if (passwdEls.reduce((a, b) => !a.value || !b.value)) {
						page.querySelector(".next").disabled = true;
						return;
					}

					passwordsMatch = passwdEls.reduce((a, b) => a.value === b.value);

					page.querySelector(".passwd-error").style.display = passwordsMatch
						? "none"
						: "block";

					page.querySelector(".next").disabled = !(
						passwordsMatch && page.querySelector("#id").value
					);
				};

				passwdEls.forEach((el) => el.addEventListener("input", handle));
				page.querySelector("#id").addEventListener("input", handle);
			},
		});

		// Connect page
		this.pages.push({
			id: "connect",
			type: "page",

			graphic: "/system/resource/app/backup/graphic-top.png",
			titleText: "Connecting to an account",
			description: "Please fill fields bellow",

			body: `
        Fill the fields:<br>
        <br>
        <div>Identifier</div>
        <input class="w96-textbox" type="text" id="id" name="id"><br>

        <div>Password</div>
        <input class="w96-textbox" type="password" id="password" name="password">
      `,
			buttons: [
				{
					class: "back",
					onclick: () => this.openPage("choose-config", []),
					enabled: true,
					text: "Back",
				},
				{
					class: "next",
					onclick: () => {
						this.openPage("server-connect", {
							type: "connect",
							id,
							password,
						});
					},
					enabled: false,
					text: "Next",
				},
				{
					class: "cancel",
					onclick: () => this.quit(),
					enabled: true,
					text: "Cancel",
				},
			],
			onopen: (page, args) => {
				page.querySelectorAll("input").forEach((el) => {
					el.style.width = "100%";
					el.style.marginBottom = "6px";
				});

				const handle = () => {
					id = page.querySelector("#id").value;
					password = page.querySelector("#password").value;

					page.querySelector(".next").disabled = !(
						page.querySelector("#id").value &&
						page.querySelector("#password").value
					);
				};

				page.querySelector("#id").addEventListener("input", handle);
				page.querySelector("#password").addEventListener("input", handle);
			},
		});

		// Server Connect page
		this.pages.push({
			id: "server-connect",
			type: "page",

			graphic: "/system/resource/app/backup/graphic-top.png",
			titleText: "Connecting to the server",
			description: "Please wait",

			body: `
        <div class="logs"></div>
        <br>
        <div class="w96-progressbar">
          <div class="progress"></div>
        </div>
      `,
			buttons: [
				{
					class: "back",
					onclick: () => this.openPage(radioActSelect, []),
					enabled: false,
					text: "Back",
				},
				{
					class: "next",
					onclick: () => {
						this.openPage("finish", []);
					},
					enabled: false,
					text: "Next",
				},
				{
					class: "cancel",
					onclick: () => this.quit(),
					enabled: true,
					text: "Cancel",
				},
			],
			onopen: async (page, args) => {
				const logs = page.querySelector(".logs");

				Object.assign(logs.style, {
					overflow: "hidden scroll",
					padding: "4px",
					marginTop: "2px",
					width: "100%",
					height: "160px",
					boxSizing: "border-box",
					resize: "none",
					color: "lime",
					lineHeight: "1.1",
					outline: "0",
					fontSize: "13px",
					fontFamily: "fixed_bmp,monospace",
					background: "#000",
				});

				const con = new KConsole(logs);
				const progress = page.querySelector(".w96-progressbar");

				const errored = (message = "Something went wrong :/") => {
					progress.replaceWith(new Text(message));
					con.error(message);
					page.querySelector(".back").disabled = false;
				};

				const success = () => {
					progress.replaceWith(new Text("Done! Click Next to continue."));
					con.println("Connected successfully");
					page.querySelector(".next").disabled = false;
				};

				con.println_styled(
					"This console will print the logs of the connection",
					"color: yellow",
				);

				if (!p3.connected) {
					con.println("Connecting to p3...");
					await p3.connect();
					con.println("Done");
				}

				con.println("Creating a connection...");
				const ADDR = (
					await fetch(
						"https://raw.codeberg.page/onofficiel/brahma/onoff_account.json",
					).then((r) => r.json())
				).address;
				const conn = await p3.createConnection(ADDR);
				con.println("Done");

				conn.on("message", async ([type, data]) => {
					if (type === "register") {
						if (data.error) {
							errored(data.error);
						} else if (data.registered) {
							conn.send([
								"generateToken",
								{
									id: args.id,
									password: args.password,
									tokenName: "OnoffAccount",
									perms: { request: ["%ALL"], read: ["%ALL"], write: ["%ALL"] },
								},
							]);
						}
					} else if (type === "generateToken") {
						if (data.error) {
							if (data.error.search("already exists") !== -1) {
								con.warn("The token already exists. Creating a new one...");
								conn.send([
									"removeToken",
									{
										id: args.id,
										password: args.password,
										tokenName: "OnoffAccount",
									},
								]);
							} else {
								errored(data.error);
							}
						} else if (data.token) {
							con.println("Importing the crypt module");
							const crypt = await include("C:/local/onoff_account/crypt.js");

							con.println("Storing encrypted token");

							const pass = args.password;
							const salt = btoa(
								String.fromCharCode(...(await cprovider.getRandomData(30))),
							);

							const key = await crypt.createKeyFromPass(pass, salt);

							await FS.mkdir("C:/local/onoff_account");
							await FS.writestr("C:/local/onoff_account/salt", salt);
							await FS.writestr(
								"C:/local/onoff_account/data.json",
								await crypt.encryptToString(key, {
									id: args.id,
									password: pass,
									masterToken: data.token,
								}),
							);

							success();
						}
					} else if (type === "removeToken") {
						if (data.error) {
							errored(`Cannot remove old token: ${data.error}`);
						} else {
							con.println("Removed old token successfully");
							conn.send([
								"generateToken",
								{
									id: args.id,
									password: args.password,
									tokenName: "OnoffAccount",
								},
							]);
						}
					}
				});

				con.println("Connection to the server...");
				await conn
					.connect()
					.then(() => {
						con.println("Done");
					})
					.catch((e) => {
						errored(e.message);
						return;
					});

				if (args.type === "register") {
					con.println("Registering new account...");
					conn.send([
						"register",
						{
							id: args.id,
							password: args.password,
						},
					]);
				} else if (args.type === "connect") {
					con.println("Creating a token for the app...");
					conn.send([
						"generateToken",
						{
							id: args.id,
							password: args.password,
							tokenName: "OnoffAccount",
							perms: {
								request: ["%ALL"],
								read: ["%ALL"],
								write: ["%ALL"],
							},
						},
					]);
				}
			},
		});

		// Finish page
		let openAfter = true;
		this.pages.push({
			id: "finish",
			type: "intro",

			graphic:
				"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKMAAAEuCAYAAADiNaIyAAAAAXNSR0IArs4c6QAAHMJJREFUeF7tXc2a2zYS1LzAJpc4VzsX+/2fJr7EZztPkNPsx9FAA0EgUF1dDYIU57IbC+if6kKjCFLUy2+/f3799f3HJf19+vrlcv73iccWfHj57+fr642J5/85EdgQgZOMG4J/ur5HYBIyLs35BauNYShm8Bw1CwIvp2YENPLfP25r5ciaelnnf254zTBJZ5xlbZ5xbInAScYb+oP3f9IdOW1LjsG+TzJejlxemAdTDDw1o0gjJUofWVMujI3M7+yMU/SEM4gFgUOR8dxw903qQ5Fxq2uRfVOAj169+E/NKNKMW9zLjdZwo+1P0xnVq0xtj+8f50wUgWnIiAasHXdSVounz9qTk9EHnnT2uS4uq5rxPDe7Ui3yXO20f4/v2Rml7e3R2ELmo//lD2N7cpWR8dxl7svwDCQsieclpYyMnhVxmLnZijzJaK/qsc4ZXy+XT9+A5xMHfOdHQUbZbrM8tzzgyyVLZ/Ro7LMz2hcwNKMko3cLg5wOHqTOcb9klLWNmAqqC3WNcq6k1Tnul4wxHJJZVRdKFpjQkDrHY2nGic4F1wrl0VSznUvWcvTkt5POONf2hDQXdddAfI4eo85xJ2QcDbPfn7pQ/oj0FtQ5nmTU1+jNorpQQWG6zKpzfCLNeN3qPZrGotl2oRmd57JPqhldC3iTyequsUkSHafqHM9tOqjKbKHYS7XaHZ/aHRFlumyOazGcZFRWJ7OlLlQtTOstR/VdIHWOT6QZr+U8ima0EnHJ3XvvuMTv1IxBnUxtVt018vgYIpb5KbpkP0eb6Jhom7YFriaP2l6/UJxHVBumHaBFXC8h1TlOREauOJGzPMtDXaja2eXySstfy+v6jFe9afh0ZDzfzxjz/KP6nNHaERPh8g6ZNGNORo+GPjVjrw1M8rm6MyrsrRGahUwRU+773KbZShi3RuWW6LGlJOTcZPSIrCBSbGVWWSglgWrakyW3MsclrvOcMehdO0rN2DqWYTSfyt6Tacai1e6o8yq7htJWdGf0lOjUjEH7uJJASlvRZPTAeZLRg15jrpJASltTkzGdM5bnUeU51R7+e+vfMUmFLs/zln9PFwmMxqudD3rqMa1mXMhYW+DsFVZQo9md2Ydulv2wkTUZ5dW0sssqbd2upnvgRD8X1/O/x8/VhVIR0m/n4xJFnePb0Y6l2GfHxNBSF8pPosfv5eTyAcvqfpQ6RzMZ83DUz8flmsujiWb4vWzlOWOZT8LGgv8amVkNu3Yh5LHnImMOCrOylHM851vKOPJFVC5cr58aoVo1sI63xjdVZ2xdlVkTK8f7yOWbTcfeeCWeSt60CIbGHRWL166kM653gI1IgVYlcJy6a+ShegjpJUwrDq9tORnLbcOjIfasISM1Y+0cE1lXFo2JaHb5vWnr1TSStForWX3OMH7WzqjU+eocb0/teFp/r/je9t2zP+Pn6kKtXRgt/97qeJEXMeocq/emI4j5bIRUFypJFuaica2e3pqoc4SeZ1R1HrVmmVlTqjUjfk741+XX939uJUuaPf/ffKv2aHq5Zvzv5yt8B0bRMb2rUbUwou2ou4bC3hqhWSwWe/l5ibe25kfIaivMmow3aKu/LcYryLN2jOLBT0lIdY5mMraEtKXoHkAtfrYaqyyUkkA17cnWQpnjEhekGXv3etmCH1lDKjVj66KF0Xwqe8Gakbtj4tGS7KpkF8CoecquobQ1dWe0XMC0CskR8vrTTUckpJpAoxaRxY86R1ozrgXNkfLjsXwLGDOP9RSK259EaBice3KsRdvRjK+XT1+Xc6uPlwshGoWF5UgaUqkZrfiPGi/XjPm9aeV2+ewdUt012AUeOW9pVPkvZHr5U31qp9ahrEl5ziPtSRn2Fmsi5PjnIOP9D7vb63YPbvcRMq+DZ+2QJxntq7hLxsWkV4PYw7rOiNKQP7//WN6z+faHaGAm/1Mz2vGFyJiTie2Uyg4536b8uNzOzmhvQWYylp0SdTlWQ6JRxY07yWjHliLjIyHxXqXskPZ0lxl4rJz9j+1JsaN4Yoieq15wNBk9mo4FKUpDMpow5bCmOU/NOEAzlkSaQUOy5I6cp+4akbGyttU5ujpjSoIh5NE1JCtHWGLMMI/hQR73271pBXBsIKxv1t+oorF5jYovwo+3JtV702ygrKYb7S9CI7Y0JJuff971qagRfwlTz7mt/NuB7OpgOwnrb0SBFh9sXqPiU/hR1WD1EbIFRPYQhAnu6BpSUfRhNtjCOwPsPs/IrmyGkJ5Owvpz4nc3vVbDjerqT2uDwKHvwLCZnRoy7t53S6OO1sQqf93OmCfNkJLtWKM7MpPbOUeLQDgZl3AZQs6vITfYx7S1n84aTMazQ05Xu8MFBGnGUhOwKJwa8tSQrXNIc2c8OyS7FM95PQSGk/G4GrIH9fl5DwGajGeH7EE71+d7uNwCNePyvr/rt0Zqez4L+6khGxrynT2ee71r9VKdC6rtuzsj2iFz4pUAM2Q+zyEZ1OaeIyWjmnAt6EaeQ+5hi5ubZlh0EjJuWayzQ2KF3sMoUDPa3rUzWpOwQLOadXR+4/21rxGi4pF0RpYMynlnh7xHc8vdiq3rociYOp0VDObeudXHOb6PwGHIiF7Vr0FyErJPlugRh9CMW987l2qob18uv/6eRKO/Xi6flniM7+dkx4d2xi11y6kho/uY3n4oGfXhtizeU3/kOeTYPI/rbSgZc4LUjlUiYD47ZASqMTbDNCPTmWp3cBT3Zlno5jqHvHZ+BR6spltwjPQv7YxsF2qRRXWVy8Sm8s0uhinnBV4IuMnIFJkF2UMOtlM3Yw0sDIvRbPMsELnIOJKICWQPIdM2YymY15/FV9xYCyXiouhZpjRjz+ioz1lNZ41vVo0VreFG2zd1xi06YY84TOdi8mD89GI/P79HACYjU8BRYDNEYfJh/IzC4Ah+IDIyhRsNDkMUS16M/dEY7N0fpBn3kmS0hmTtn5oTu9fe7IyWztEj7NqBdm+e6fOXy91DBshca4576pD7uIb+qFLz/YxIMVtjLLf8mHPANd8WwkBkzF4Aa7Htxe/Z5svfXLsAqCgYRJJGtSwxWHxZ7D4bmbz5TvFO756mYpO0aDyWkJH3akef823t76EzWoqSkySyY4yI6d5H+8XskbmyC+8I8yRkHFEchpCWuCz2LXZ5kuzt8oPPNM28I6OlIMnAr+8fX2v0h9O3YI3RQhyLbYvdflaTjNiY/zfNaCnEBxF/hD7ftqZhrKXradLyc8R+mnNqRt13ZG6d0UrGLTtDZKyo7S3zRxbLHsdQZJyhEChprEdNUXb3SI7RMb+RMa4AsSIkKm7U7gyLcjRhVv0JSm36VVXLuV0KeoSmQguCxq+2Z9WsTzn+9XIxdcYZO0FEF4uwiRJ8j+PyW7mWW8BlrubOOBtYKHEs2rFms7YLzbg41fVRPDOAEhQm48zAo4REc0DtWQiuJskIexYcrPHUagGTMQFv1YB/fP1y+Tf4XS0oEKhmTON6di32RmpoRnMqOmAPr/LzEj+YjGhXsQakGI+uYDQH1N7eO6MlT0WdWjaW2kBkRIsYHXDLPgKsJQ+1vS2xWXxv0fmsOUNk3LIDIMdXCHESMAgh1fasRUFyRmxa8kDsRY+ByMhokNEaCQHeovHU9qLxGNH5cvzK45wyPyYeiIxbdsbWakwdBCHOnjoj2oEseaM218YhO8raXJSYEBk9gXhBQOcjhbHkobaH5uEtqNdPajzlqYnCbk+7QmSctTPmWwMKFkJIhIiWTovG1iKi18Zt/spD7AguphgA4VvibCaj9Zxx1HiEQHvSjEg+JnJkg2vaL1rTtuynz8xkZAGInIcWDl39qD31jmHxa8UTzd1qVzV+yR0moxp4VRJJhyD2LAVBiGGx14sP8dezkUuHKM1XxrB2VY3GmndMkIwvl+t3Xeb8QwuJkkdtz6sFAfl1c4Hm6Kkkis9dAwOSAMl4Dd2iuUZqEBRYNH61vZ/Lra53o+gxBxIDmo/inDgREODUW+j5OFSjns8zVqqOrny2C6H2a4RkfSLkro3xxGqN/2nIeLdlNCpjAZ8hhsV+rgFZMrHzLHGi3bKXj4mMaEFZAFrzaglbALMQB7VrsbnkhtrdEmdrnJ5al/hR35tWaBCvpmQLi5x7ogBbNBsSr8WeFb+1q97rv9//vvRj/u3XvaB4lePKfNe/qtrxb+0KbMBWHeP9egBCGmvnQmwq8Vy7SOr5QOJcq2MiFlPnFBf1vene3s8E9DYHEB9twOorqFeEPF6kIFvaW8MWuUpvxY3kbak7Ek9pT/CunY9X5NIkNEy0gBbRwawF6aVmIXZpy4KFgohsrEici23J+xlHaEjLSssJg2hE6xaD5IsWwBKfNc7awqn5e1gwxQaD5NvTsAgeklfiWTtQr1t4OkA+17qSc8BaiqFnFwG+Z8MqHVqYerqiJc5eXXu4yMgYRcheAmwRPIRHClTGXd6RQArHdsLSNktGJM9eHnefv14un759WZ0if6e3MoGjEJHp1p7cUX89H8pa9jv9S10zKlckqolyzWFabZXBVo2D+kPPAdfseeczcSL4e+O14r3Gr2G/A1O7qOitTBR8tBPU7FliQDpFy15vviUWy1a8hqMnVqY2ve7Y/bk2D0DegK3ze8WOJmMPK6t2W7uIqnVYK1ZbEnGJtea/S8a1idbko8dHE3GJv+fDU+D1uffnLKsxADcMep0p372i61XjFfTbgTYNGXMfswWOSrMgPtY0GEpE6JxvJRBUcyJ4eOJF7OfXAL3xaSzUGXPDqhVjXMirbnvdyrs1o90CLS4TD5Njr06eeHu20c/LGExkTK3V1inR0PBxyX/+BDU6u6franYQMrDFReJB/KP5I40lwt9afHn+ZjKiusMKDjreCxRS/DwWxF8UERHfKG7lODZm1h+yI8CasaUBegEqt2Tk3AzVKL24LRoNLa5FM9b8q9536Ym3h6/185tm/O33z6+qFWjtOj0yLJ9vGRvqGy3sFN3pvTOwMSM1Q8c8aMZExnLFogZrAOcrmiGo4hzNUvhlLPtQLltUdh5bFwse6CL0xFI9Z1zImIzCQZD7biJ8+b+epHpzmcVg7cgMsSxzSLh70DS/kwNzoetlfUC1M5bDrXv+jONzwlvxsuazZr+lOVEyqjRiTfMjMSg0eg3Pbme0XkXiRY5a2/UI2G5o7Yh5gWuRtLoLQgQcX27kVjG8PUZ2248/Ym++UWJEq+ZgxFu/xT6TL1tQdp4lH2RsLY7UOhg8ej5beXdfb9IPaGzXayUb1xHbOa75ZTpjH+9euW2f9zBTx+MiY7l1R2mImqZZ/g311wO1VSKrRizHR2pGNH8Pfj36evF5wGvl8YVuZ2xd3PSSWP/89fJ6ebm9DIm14yFg8qlY+crOyOpWFsO04K/z1x9y8eKE1MpMxgRWuWI9YFjmeq6SIxYWqv3KjR6dZ8HGMxYhC3P+i9hNcVNkrCXtXTk9IC1J9Wypuw9DLGYOkpdnjAVjpN5WexAZ0UuU8s5L65ytp3EeOqDwMUm5BlphgOKccYRmRDVwj+gp3964tR0KIqPV+FrnzAn2GLiQbY2AkRVtzZftcuw8a3zW8ZaOZrXdkkrUW8i8AWw1P4KIeYe3ypde0SPj7dWgF1tvPvJ5mZ/7XTuI063HRBeV7XC9gkfH3atLL77e/LXP1/KSvGuHDSp6nkez9jRt+TlKyJoGbBVtC82o0pC1LbmVj/yNEikA6KInUCaO7iooGWvE63Wg0bmsLY5enK3mguTQ/dqBJ4DoztcSw6N9e8i4xIrgjBS0RaTytCONZezWLkZrpymWOnTJ2BPoFmeRYxlAZfH0np5+uVx+/d1+jyVCxiVeJs+ebcamDLvMEPUdmIhAGJsjNaFFQzIasEeYms3Wua7XniVfxbntEi/cGRmtwxCsN2eWVczg0YudIVAPL+vnvRit9nrj85xdZMwdtQ+0eyHdf+7VHjZv2tEjtKM24kdrIwhZw0lGRgSgnuhFbOxhjJeQS45bdcloIrbyojSjSyN8+3In5mc4R3Pl8/Y7Kh8XJwiJrP6iFmBXc7+/adYab208gsvQzhgF6lC7wAEqArylAyH2UAwsflGb5Tg23pOMLOKdeUhBLMTIJQ4acq3zdecCi23tWqFruzPgJKMXwcZ8NSFrHWiLiz0kLwbWeM34BBqxp6l6henNn+FzpjP38k6fp/zOzogi5hiHdBLLlu0IBZqKEM+4m1f9ljmvktFzbti6MwChccBBCCHLTjESBoSA3nh6Gnaz5xl7gXkT18x/X/+KNmA8O4zslCmdhwUS8BSVpc7DvnaAkKN77mX4HnXyN+M55hsZgMIr8diq81nwn4qMOWFDOoOowyELqzXGsmWXdiydxuPHmqOpXit1mJaMJTHLFVZ9o+IkZEMKqSRKIijiVzHGsiAs/nZBxlpCppVoQWTgWCUho8MegfduyfgB/svl1/d/bu/ksWiUxcYs42ciZusgPRKvA5Cx6AnvFwYjVrK6G/UIGalCZsBrv2QErkbzc7tHzammksbep69/3Tq9xuKjlSjN5413v2QkM5+hA1hD73VMi72Z8i87/dORsXeVHqmJlBq1docMufM1c35PTca7jpJpzb1s6ZaOuIexJxkbVZpVW+2BWGWMyMWXmYzIZb9S46DAI8mitlrjJJprVLCKhAfauCNjnOZYzgGXq8Qfm33RKAJT5b3jFN/Mmk6peWv53sgoWfFExe1d1HCmQ8TjnbIVjt64Y+bbtoBpHq4d8VRJDODtc7zzYghHnSCjje14KI8j7V3T423M3LNzruMc9B2Yf95/xkF37/dZOudTa8b/fr5WfsVtTJfwejlS5zw75sX34icvmZTzj9o5lRjNbovQjLOndI1vWNcMvrh/po4ZpBnv3z2TA7qVJjpC55z9XHPRe39W3j2E1v+wnbHVv4d1zcBN5Igd8ynJmHNkzx1zbkLajwBXyWg3FdgGBpveY+ecm5j3BVzjFq0Ze8/THe1e6x4IOrum7N3b7m7TqiLsaeX2GrEKk54f9vO9Yv1AxkgN1Xr8jAV+63mReLG57fU5zDcybrnS97qKa0SZlZgsqUfPMz9cGxng3jVProlmIiZ6ztfTdNGfT0XGGtGP1jl1i9l262cPOE5Pxrx4e9VCs2zpsxNyV2TcvHMGH76O0O4zE3L3ZOx1zq3uhXvPWaOJmUg5Ez6HImNJTJ0+28bSKEJuk92j18OS8SjEHHFVPsvWfWgyzgKyqvNEdcpZcNonGTunGns5V2PP7VTkru0em2nI18vlcGScZZVHEKa0qe6UW2NnIiNyb3mEximLcqTzRyuJlYSskdF+mmWfkXLuklGxWpSA1YhoLeBM4/nSfWShxFdRbxbfKhlHaC4FgEe6l604l0x4sGRI80bUv5bvAxm3WBkMMbeI01vkEfMZLDe/s/UewO1J7/Iq6u1zxR6CVCDzg2jOkUSsPdG+llJKA9HWCCzMGAQ/1O5InJeYuk96o4FHjstX+wiAVN1ly46jyiEa77zf0d+B8Wqc2eYrO8oDCVfORUdpXm+jGKUhd9EZvWD25qu6SM9P6/OoDqTILSq2Eo+nJaOiSB7yrW3hVe3ucKTo+Lsm46jrHrZGMxKxzEVNAG/Od/EEFfgpNeN94W2P73cXgNCcUlN6ybjkrYynds3wFNu0ohBdEgYNUHZILw7KWGpwHZ6M3gIEccxkVnnv3YtHJCH3S0ZAt3iBNzFmwGAVETy4qGKodsbffv/8OuocaQlg1PNyHsAH8IpyodRsED6N89GIc+L9dsZGOSGgKTrMMUnRnbwYKWI4/DmjF+Q56IZF4SGEByeP31Zmh+qMHoCx8tePN8ota0Qc6agFjbs2DouzvldHEPIw54wYsLbSRWs04BqsGbAqPhsqH6Or/r99ufz6m3un+247Y17INhG5U+iIlZ8u4Njil/PyR9Y8NtmFrMZot2TMt0ZPIWpz1SDzWySWmTfek4wYzt1RLJClYW9Bu4FWBqhi9+pHTxxK3A6hGa915rbjspCjzkEjOrtHQ6oI6cFv19u0B8BEBuXKZrpjPseXz3UxYvk8XjqxvjF/GDK7JSMLXg4LDaT3MrhRG29e1pxSKh6/Vp9r6e+SjB7gZuyIZXFu+RHKw0MMFlePzzz3XWpGrOmvj9rDvXiWGAoNbMVXhefTdUbVKrYWzDreQ0bP1TXrV4Hr7sjIguUpkJVIivGePD1ShPU7hIyBWp2qGQvW3si4xLtVroxfCRn3+Dwjw+IEluccLBFEpZFQe0y+afEx+Y72l/Dc1TbNrFjPlsUWRT2PzZvtVg/+gKt61tfd1fTyc21q8Nbt8Zs+W5CZt2cLGkz+HoKM9rfUaTedkQFnZiJaGgCbO5s/689D/jcy7kUzsgApzt1Ga8SaPyb/0feqPf7OzmhpTxOMZQlpDZ3xw3bhDTWjFZaP8QxA3m2Dj1Y/k8nfc/HG+PPivQvNyACjWKl6Stkt4k+0122zBHnEHLikfn8Fij3L64xDa0avhlkA+jinu9KCObdTa05rsRn/TAPw4n12RmtlNxzPEITdIUb6SpDugoxLT/rz6xczDdgtyuxo4ASGJAwOo/zs7gLmHph47TKQWyZXDEF21Rn3cM5oqlg22Kth7jXj1fCpGderUcP7j69fLv9+x75HvYttmukIzNbEkn7kvJFYjPT1djU99t40VzYGFHZ74iIcN4vBglmYo/zsXDNihWcKgFnebhRDEHZRjvR1u5p+ds1o0TSnZmwvRK9G923TlmegHA2FWaVvLx9aLrwP9MfgIOmM2AHGG9KeHenhDsystWMK4QHm2XHYAu+7X1WdtXgMMN5V2iPjoE3hLoyROIz0dacZS+CZe5nJRtQ5XBuc9RdaRsUTnW/NvoIgKB6ML7dmXC5gal0gvEsaWgsDTMopPI9eCxV+zuDQy3+tDBG+elA8/Pj5GjHLFdUzrP58C3DYHAzrrO2i+B1uNp4eIWt2t8AbIuMmnbNwyoATrRtZcsDzBGQcRUQF1jQZc0BzrYBqEqvmYslYghQVnzUf63gmf4+GG+1vwUNCxlGdkwFIsWLhThY0cHTeo/0l2MLIuNY5PfUaA5JM8XlSvc1lc2YX4eKPRYCRBDlIQ8hYEpOt0ujCsHE+zmPLu837dhicvUQM3aZbhfSeY7Ik8Wgoq8ZTjh+dL+Mv1dSjyYd3xpQou5KYVavqzEyRPHM8ufrwNdyMfk+Q9bfpNu0lxrVAdrC8fj2kYub6ichJA8avgoiCbVpACgIzBrA9kXGr/Fi/k5CxWPNGbno03D1wRsfZo04ejbNkHzHf0klra3m0Jvf6S/PfnmdkV4QFtNrYBcj8yzoWe4qYVSvaEndr7NY5sf5VON4erk0rnA2IKYgniXqc9g65xH0fB6EbmOSLOQrc9XhiiXn83l3AtL6QtQaQslyeRBQF9F7dY+Vqj1LlsQWWHp8lKvC7diI7p1dzKAiROmSEBuzldx+/r7uz8TMLwqP5U855vNR3YCKI6VlhDJAtAntiQRaGOt5HqYFE8TGGjaeOE79vUmSspfpAUMMCVxSfBXQUKSMWsEJieHBT1A3WjLb1tbLSQFJ6E/OAiuTpiS8yNk9cS96e2Ly+ac1Y2+NTMj1NlGuLVvIqDYKQyzOmzAfNz+OzNndLvFLNWY0q04xeUHuE9Nj3rHSP39FzFV3Jg5XC/0Nn3PpdO6WW8iYZqc10hAO1y4pDL0azbc8pTdkFTA03/rrq3hpjx7PqdaTTW9qaiN4r9xYiL//7/fMr+v48i0b0akzlfD0ltrG4pUbMM0auEZj6hXZGecmYFum8YpTnQBpUdETv9hzZFRfb/wcYczmUhAtwqwAAAABJRU5ErkJggg==",
			titleText: "Configuration successfully finished",

			body: `
        The configuration is done. You can now access your account. <br>
        <br>
        <i>
          Note: Everything is encrypted so you will need to type your account
          password every time the application opens.
        </i><br>
        <br>
        <div class="w96-checkbox"></div>
      `,
			buttons: [
				{
					class: "back",
					onclick: () => {},
					enabled: false,
					text: "Back",
				},
				{
					class: "finish",
					onclick: () => {
						if (openAfter) w96.sys.execCmd("onoff-account", []);
						this.terminate();
					},
					enabled: true,
					text: "Finish",
				},
			],
			onopen: (page, args) => {
				page.querySelector(".explain").innerText =
					"Click Finish to close this wizard.";

				const checkbox = new CheckBox({
					label: "Open Onofficiel Account after closing",
					checked: openAfter,
					onchange: ({ checked }) => {
						openAfter = checked;
					},
				});

				page.querySelector(".w96-checkbox").replaceWith(checkbox.getElement());
			},
		});

		this.openPage("intro");
	}
}
