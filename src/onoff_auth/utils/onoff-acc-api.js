const { p3 } = w96.net;

class OnoffAccount {
	token = null;
	apiAddr = null;

	constructor() {}

	async connect(token) {
		let resolve = null;
		let reject = null;
		const promise = new Promise((res, rej) => {
			resolve = res;
			reject = rej;
		});

		this.apiAddr = (
			await fetch(
				"https://raw.codeberg.page/Onofficiel/Brahma/onoff_account.json",
			).then((r) => r.json())
		).address;

		this.token = token;

		// Config p3
		if (!p3.connected) await p3.connect();

		this._conn = await p3.createConnection(this.apiAddr);

		const handleMsg = ([type, data]) => {
			if (type !== "check") return;

			this._conn.removeEventListener("message", handleMsg);
			if (data.error) reject(new Error(data.error));
			else if (data.ok) {
				resolve();
			}
		};

		this._conn.on("message", handleMsg);
		await this._conn.connect();

		this._conn.send([
			"check",
			{
				token: this.token,
			},
		]);

		return await promise;
	}

	async #handleReq(type, data) {
		let resolve = null;
		let reject = null;
		const promise = new Promise((res, rej) => {
			resolve = res;
			reject = rej;
		});

		const handleMsg = ([t, d]) => {
			if (t !== type) return;

			this._conn.removeEventListener("message", handleMsg);
			if (d.error) reject(new Error(d.error));
			else resolve(d);
		};

		this._conn.on("message", handleMsg);
		this._conn.send([
			type,
			{
				token: this.token,
				...data,
			},
		]);

		return await promise;
	}

	async generateToken(name, perms) {
		const res = await this.#handleReq("generateToken", {
			tokenName: name,
			perms,
		});

		return res.token;
	}

	async register(id, password) {
		await this.#handleReq("register", {
			id,
			password,
		});
	}

	async unregister(id, password) {
		await this.#handleReq("unregister", {});
	}

	async removeToken(name) {
		await this.#handleReq("removeToken", {
			tokenName: name,
		});
	}

	async getTokens() {
		const res = await this.#handleReq("getTokens", {});
		return res.tokens;
	}

	async renameAccount(newId) {
		await this.#handleReq("renameAccount", { newId });
	}

	async changePassword(newPassword) {
		await this.#handleReq("changePassword", { newPassword });
	}

	async getKeys() {
		const res = await this.#handleReq("getKeys", {});
		return res.keys;
	}

	async getUID() {
		const res = await this.#handleReq("getUID", {});
		return res.uid;
	}

	async changePerms(name, newPerms) {
		await this.#handleReq("changePerms", {
			tokenName: name,
			perms: newPerms,
		});
	}

	async readKeys(keys) {
		const res = await this.#handleReq("readKeys", {
			keys,
		});

		return res.keys;
	}

	async readKey(key) {
		const res = await this.#handleReq("readKeys", {
			keys: [key],
		});

		return res.keys[key];
	}

	async tokenCount() {
		const res = await this.#handleReq("tokenCount", {});
		return res.count;
	}

	async keyCount() {
		const res = await this.#handleReq("keyCount", {});
		return res.count;
	}

	async writeKeys(keys) {
		await this.#handleReq("writeKeys", {
			keys,
		});
	}

	async writeKey(key, value) {
		await this.#handleReq("writeKeys", {
			keys: { [key]: value },
		});
	}
}

module.exports = OnoffAccount;
