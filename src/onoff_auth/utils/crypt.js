const { cprovider } = w96.sec;

async function encryptToString(key, str) {
	const enc = await cprovider.encrypt(key, str);
	enc.data = Array.from(new Uint8Array(enc.data));
	enc.iv = Array.from(enc.iv);

	const decoded = JSON.stringify(enc);
	return btoa(decoded);
}

async function decryptFromString(key, str) {
	const enc = JSON.parse(atob(str));

	enc.iv = new Uint8Array(enc.iv);
	enc.data = new Uint8Array(enc.data).buffer;

	return await cprovider.decrypt(key, enc);
}

async function createKeyFromPass(pass, salt) {
	salt = new Uint8Array(Array.from(atob(salt), (c) => c.charCodeAt(0)));

	const km = await cprovider.passwordToKM(pass);
	const key = await cprovider.deriveFromKM(km, salt);

	return key;
}

/**
 * Hash a string and return it as a hexadecimal string
 *
 * @param {string} text The string to hash
 * @returns The hashed hex string
 */
async function hash(text) {
	const hashBuffer = await cprovider.simpleDigest(text, "SHA-512");
	const hashHex = cprovider.buffer2hex(hashBuffer);

	return hashHex;
}

module.exports = {
	encryptToString,
	decryptFromString,
	createKeyFromPass,
	hash,
};
