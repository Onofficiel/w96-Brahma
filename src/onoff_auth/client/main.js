const { p3 } = w96.net;
const { TabControl, GroupBox, ListView } = w96.ui.components;
const { DialogCreator, Theme } = w96.ui;
const { cprovider } = w96.sec;

class OnoffAccountApplication extends WApplication {
	constructor() {
		super();
	}

	async main(argv) {
		super.main(argv);

		if (!(await FS.exists("C:/local/onoff_account/data.json"))) {
			w96.sys.execCmd("onoff-account-setup");
			this.terminate();
			return;
		}

		// Modules
		this.crypt = await include("C:/local/onoff_account/crypt.js");
		const OnoffAccount = await include(
			"C:/local/onoff_account/onoff-acc-api.js",
		);

		// Start P3
		if (!p3.connected) await p3.connect();

		// Get password
		{
			const pass = await this.getPassword();
			if (pass === null) this.terminate();

			this.salt = await FS.readstr("C:/local/onoff_account/salt");
			this.data = await this.decryptData(pass, this.salt);
		}

		// Setup API
		{
			this.acc = new OnoffAccount();
			const dialog = DialogCreator.progress("Connecting to server...", {
				title: "Onofficiel Account",
				icon: await Theme.getIconUrl("apps/p3"),
			});
			await this.acc.connect(this.data.masterToken);
			dialog.close();
		}

		// Create window
		this.mainwnd = this.createWindow(
			{
				title: "Onofficiel Account",
				bodyClass: "onoff-account",
				body: `
          <style>
            .onoff-account .w96-tab-control>.content {
              padding: 15px;
            }
            .w96-tab-control .content .buttons>button {
              width: 80px;
              height: 23px;
              margin-left: 4px;
            }
          </style>
          
          <div class="appbar"></div>
          <div class="tabs"></div>
          <div class="w96-footer">
            <div class="cell uid"></div>
          </div>
        `,
			},
			true,
		);

		await this.configWindow();
		this.mainwnd.show();
	}

	async decryptData(pass, salt) {
		const key = await this.crypt.createKeyFromPass(pass, salt);

		const data = await this.crypt
			.decryptFromString(
				key,
				await FS.readstr("C:/local/onoff_account/data.json"),
			)
			.catch(async () => {
				pass = await this.getPassword();
				return null;
			});

		if (data === null) this.terminate();
		if (data) this.key = key;
		return data ? data : this.decryptData(pass, salt);
	}

	async getPassword() {
		let resolve;
		const promise = new Promise((res) => {
			resolve = res;
		});

		if (!FS.exists("C:/local/onoff_account/data.json")) {
			w96.sys.execCmd("onoff-account-setup");
			this.terminate();
			return null;
		}

		const dialog = DialogCreator.prompt(
			"Please enter your account password:",
			{
				title: "Onoffciel Account",
				icon: await Theme.getIconUrl("devices/hdd-encrypted"),
			},
			async (input) => {
				if (input === null) this.terminate();
				else resolve(input || "");
			},
		);
		dialog.wnd.getBodyContainer().querySelector("input").type = "password";

		return await promise;
	}

	async writeData() {
		return await FS.writestr(
			"C:/local/onoff_account/data.json",
			await this.crypt.encryptToString(this.key, this.data),
		);
	}

	async configWindow() {
		const body = this.mainwnd.getBodyContainer();

		// Footer
		body.querySelector(
			".w96-footer>.uid",
		).innerText = `UID: ${await this.acc.getUID()}`;

		const tabs = new TabControl();

		tabs.openPage(
			tabs.addPage("Account", async (page) => {
				const info = new GroupBox({
					title: "Account Info",
					body: `
            <b>User ID:</b> ${this.data.id}<br>
            <b>Password:</b> ${this.data.password.replace(/./g, "*")}<br>
            <b>UID:</b> ${await this.acc.getUID()}
          `,
				});

				const infoEl = info.getElement();
				infoEl.style.marginTop = "8px";

				// Settings
				const settings = new GroupBox({
					title: "Settings",
					body: `
            <button class="w96-button change-id">Change User ID</button><br>
            <button class="w96-button change-password">Change Password</button>
          `,
				});

				const settingsEl = settings.getElement();
				settingsEl.style.marginTop = "8px";

				settingsEl.querySelector(".change-id").addEventListener("click", () => {
					const dialog = w96.ui.DialogCreator.fieldset(
						[
							{
								type: "text",
								raw: true,
								value: "Change your user ID",
							},
							{
								type: "spacer",
							},
							{
								type: "textbox",
								caption: "ID:",
								name: "id",
							},
							{
								type: "textbox",
								caption: "Password:",
								name: "password",
							},
							{
								type: "textbox",
								caption: "New ID:",
								name: "new-id",
							},
						],
						null,
						async (e) => {
							if (!e) return;

							const id = e[0].value;
							const password = e[1].value;
							const newID = e[2].value;

							if (!id || !password || !newID) return;

							if (id !== this.data.id || password !== this.data.password)
								return alert("Incorrect.", { icon: "error" });

							await this.acc
								.renameAccount(newID)
								.then(async () => {
									this.data.id = newID;
									await this.writeData();

									console.log(this.data);

									alert("ID successfully changed.");
								})
								.catch((e) =>
									alert(`Cannot change ID: ${e}`, { icon: "error" }),
								);
						},
					);

					dialog.wnd
						.getBodyContainer()
						.querySelector("[fname='password']").type = "password";
				});

				settingsEl
					.querySelector(".change-password")
					.addEventListener("click", () => {
						const dialog = w96.ui.DialogCreator.fieldset(
							[
								{
									type: "text",
									raw: true,
									value: "Change your user password",
								},
								{
									type: "spacer",
								},
								{
									type: "textbox",
									caption: "ID:",
									name: "id",
								},
								{
									type: "textbox",
									caption: "Password:",
									name: "password",
								},
								{
									type: "textbox",
									caption: "New password:",
									name: "new-password",
								},
							],
							null,
							async (e) => {
								if (!e) return;

								const id = e[0].value;
								const password = e[1].value;
								const newPassword = e[2].value;

								if (!id || !password || !newPassword) return;

								if (id !== this.data.id || password !== this.data.password)
									return alert("Incorrect.", { icon: "error" });

								await this.acc
									.changePassword(newPassword)
									.then(async () => {
										this.data.password = newPassword;
										this.key = await this.crypt.createKeyFromPass(newPassword, this.salt);
										await this.writeData();

										alert("Password successfully changed.");
									})
									.catch((e) =>
										alert(`Cannot change password: ${e}`, { icon: "error" }),
									);
							},
						);

						dialog.wnd
							.getBodyContainer()
							.querySelector("[fname='password']").type = "password";
						dialog.wnd
							.getBodyContainer()
							.querySelector("[fname='new-password']").type = "password";
					});

				// Risky Zone
				const risky = new GroupBox({
					title: "Risky Zone",
					body: `
            <button class="w96-button delete-acc">Delete Account</button>
          `,
				});

				const riskyEl = risky.getElement();
				riskyEl.style.marginTop = "8px";

				// Delete button
				riskyEl.querySelector(".delete-acc").addEventListener("click", () => {
					const dialog = w96.ui.DialogCreator.fieldset(
						[
							{
								type: "text",
								raw: true,
								value:
									"You will delete your account. <b>This cannot be reversed!</b>",
							},
							{
								type: "spacer",
							},
							{
								type: "textbox",
								caption: "ID:",
								name: "id",
							},
							{
								type: "textbox",
								caption: "Password:",
								name: "password",
							},
						],
						null,
						async (e) => {
							if (!e) return;

							const id = e[0].value;
							const password = e[1].value;

							if (!id || !password) return;

							if (id !== this.data.id || password !== this.data.password)
								return alert("Incorrect.", { icon: "error" });

							await this.acc
								.unregister()
								.then(async () => {
									await FS.rm("C:/local/onoff_account/data.json");
									await FS.rm("C:/local/onoff_account/salt");

									alert("Account deleted successfully");
									this.terminate();
								})
								.catch((e) =>
									alert(`Cannot unregister account: ${e}`, { icon: "error" }),
								);
						},
					);

					dialog.wnd
						.getBodyContainer()
						.querySelector("[fname='password']").type = "password";
				});

				page.appendChild(infoEl);
				page.appendChild(settingsEl);
				page.appendChild(riskyEl);
			}),
		);

		tabs.addPage("Tokens", async (page) => {
			Object.assign(page.style, {
				display: "flex",
				flexDirection: "column",
			});

			const list = new ListView();
			list.setSelectionMode("full");

			const listEl = list.getElement();
			Object.assign(listEl.style, {
				flexGrow: "1",
				overflow: "hidden scroll",
			});

			const refreshList = async () => {
				list.clear();

				list.addColumnHeader("Name");
				list.addColumnHeader("Request permissions");
				list.addColumnHeader("Read access");
				list.addColumnHeader("Write access");

				const tokens = await this.acc.getTokens().catch(() => null);
				if (!tokens) {
					alert("Unable to fetch tokens");
					return;
				}

				for (const [name, data] of Object.entries(tokens)) {
					list.pushData([
						name,
						data.perms.request.includes("%ALL")
							? "All requests"
							: data.perms.request.join(", "),
						data.perms.read.includes("%ALL")
							? "All keys"
							: data.perms.read.join(", "),
						data.perms.write.includes("%ALL")
							? "All keys"
							: data.perms.write.join(", "),
					]);
				}
			};

			refreshList();
			page.appendChild(listEl);

			const buttonsEl = document.createElement("div");
			buttonsEl.classList.add("buttons");
			Object.assign(buttonsEl.style, {
				marginTop: "10px",
				textAlign: "right",
			});

			// Buttons
			const refreshBtn = document.createElement("button");
			refreshBtn.classList.add("w96-button");
			refreshBtn.innerText = "Refresh";
			refreshBtn.addEventListener("click", refreshList);

			const removeBtn = document.createElement("button");
			removeBtn.classList.add("w96-button");
			removeBtn.innerText = "Remove";
			removeBtn.addEventListener("click", async () => {
				const selected = list.getSelectedItem();
				if (
					!selected ||
					!(await new Promise((r) =>
						w96.ui.DialogCreator.confirm("Are you sure?", {}, (e) => r(e)),
					))
				)
					return;

				const tokenName = list.getSelectedColumnData(0);
				await this.acc.removeToken(tokenName);
				selected.remove();
			});

			buttonsEl.appendChild(refreshBtn);
			buttonsEl.appendChild(removeBtn);

			page.appendChild(buttonsEl);
		});

		tabs.addPage("Keys", async (page) => {
			Object.assign(page.style, {
				display: "flex",
				flexDirection: "column",
			});

			const list = new ListView();
			list.setSelectionMode("full");

			const listEl = list.getElement();
			Object.assign(listEl.style, {
				flexGrow: "1",
				overflow: "hidden scroll",
			});

			const refreshList = async () => {
				list.clear();

				list.addColumnHeader("Name");
				list.addColumnHeader("Value");
				list.addColumnHeader("Readable By");
				list.addColumnHeader("Writable By");

				let keys = await this.acc.getKeys().catch(() => null);
				const tokens = await this.acc.getTokens().catch(() => null);
				if (!tokens || !keys) {
					alert("Unable to fetch keys");
					return;
				}
				keys = Object.fromEntries(
					Object.entries(keys).map(([k, v]) => [
						k,
						{
							value: v,
							read: [],
							write: [],
						},
					]),
				);

				for (const [name, data] of Object.entries(tokens)) {
					if (data.perms.read.includes("%ALL")) {
						for (const key of Object.keys(keys)) {
							keys[key].read.push(name);
						}
					} else {
						for (const key of data.perms.read) {
							if (!keys[key]) keys[key] = { value: null, read: [], write: [] };
							keys[key].read.push(name);
						}
					}

					if (data.perms.write.includes("%ALL")) {
						for (const key of Object.keys(keys)) {
							keys[key].write.push(name);
						}
					} else {
						for (const key of data.perms.write) {
							if (!keys[key]) keys[key] = { value: null, read: [], write: [] };
							keys[key].write.push(name);
						}
					}
				}

				Object.entries(keys).forEach(([key, data]) => {
					list.pushData([
						key,
						data.value.toString(),
						data.read.join(", "),
						data.write.join(", "),
					]);
				});
			};

			refreshList();
			page.appendChild(listEl);

			const buttonsEl = document.createElement("div");
			buttonsEl.classList.add("buttons");
			Object.assign(buttonsEl.style, {
				marginTop: "10px",
				textAlign: "right",
			});

			// Buttons
			const refreshBtn = document.createElement("button");
			refreshBtn.classList.add("w96-button");
			refreshBtn.innerText = "Refresh";
			refreshBtn.addEventListener("click", refreshList);

			const addBtn = document.createElement("button");
			addBtn.classList.add("w96-button");
			addBtn.innerText = "New Key";
			addBtn.addEventListener("click", async () => {
				w96.ui.DialogCreator.fieldset(
					[
						{
							type: "textbox",
							caption: "Name:",
							name: "name",
						},
						{
							type: "textbox",
							multiline: true,
							caption: "Value:",
							name: "value",
						},
					],
					null,
					async (e) => {
						if (!e) return;

						const name = e[0].value;
						const value = e[1].value;

						if (!name || !value) return;

						let pValue = null;
						try {
							pValue = JSON.parse(value);
						} catch {
							alert("Bad value", { icon: "error" });
						}

						if (pValue === null) return;

						await this.acc.writeKey(name, pValue);
						refreshList();
					},
				);
			});

			const removeBtn = document.createElement("button");
			removeBtn.classList.add("w96-button");
			removeBtn.innerText = "Remove";
			removeBtn.addEventListener("click", async () => {
				const selected = list.getSelectedItem();
				if (
					!selected ||
					!(await new Promise((r) =>
						w96.ui.DialogCreator.confirm("Are you sure?", {}, (e) => r(e)),
					))
				)
					return;

				const keyName = list.getSelectedColumnData(0);
				await this.acc.writeKey(keyName, null);
				selected.remove();
			});

			buttonsEl.appendChild(refreshBtn);
			buttonsEl.appendChild(addBtn);
			buttonsEl.appendChild(removeBtn);

			page.appendChild(buttonsEl);
		});

		// Tabs finishment
		const tabsEl = tabs.getElement();
		tabsEl.style.flexGrow = "1";

		body.querySelector(".tabs").replaceWith(tabsEl);
	}
}
