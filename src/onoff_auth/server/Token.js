class Token {
  perms = {
    request: new Set(),
    read: new Set(),
    write: new Set(),
  };

  constructor(perms, hash = null) {
    this.changePerms(perms);
    this.hash = hash;
  }

  toObject() {
    const { hash } = this;
    const perms = Object.fromEntries(
      Object.entries(this.perms).map(([k, v]) => [k, Array.from(v)])
    );

    return {
      hash,
      perms,
    };
  }

  static from(obj) {
    const { hash, perms } = obj;

    return new this(perms, hash);
  }

  changePerms(perms) {
    this.perms = {
      request: perms?.request?.length
        ? new Set(perms.request)
        : this.perms.request,
      read: perms?.read?.length ? new Set(perms.read) : this.perms.read,
      write: perms?.write?.length ? new Set(perms.write) : this.perms.write,
    };
  }

  async generate() {
    const uuid = crypto.randomUUID();
    this.hash = await hash(uuid);

    return uuid;
  }
}
