const { p3 } = w96.net;
const { term } = env;

class OnoffAuthServer extends WApplication {
	constructor() {
		super();
	}

	server = null;
	resolve = null;
	database = null;

	async updateDatabase() {
		await FS.writestr(DATABASE_PATH, JSON.stringify(this.database.toObject()));
		return this.database;
	}

	getAccount(id) {
		const acc = this.database.accounts.find((acc) => acc.id === id);
		return acc ? acc : null;
	}

	async findUserFromToken(token) {
		const hToken = await hash(token);
		return (
			this.database.accounts.find((usr) => {
				for (const [n, t] of usr.tokens) {
					if (t.hash === hToken) {
						return true;
					}
				}
				return false;
			}) || null
		);
	}

	getTokenFromHash(user, hash) {
		for (const [n, t] of user.tokens) {
			if (t.hash === hash) {
				return t;
			}
		}

		return null;
	}

	async handleReq([type, data], socket) {
		function emit(type, data) {
			return socket.send([type, data]);
		}

		const writeKey = async (user, key, value) => {
			await user.writeKey(key, value);
			await this.updateDatabase();
		};

		/**
		 *
		 * @param {boolean} needPerm Tells if the token perms need to include the request name
		 * @returns {Promise<import("./Account.js").Account | null>}
		 */
		const handleAccess = async (needPerm = true) => {
			let user = null;
			let error = null;

			if (data.id && data.password) {
				user = this.database.accounts.find((user) => user.id === data.id);
				if (!user) error = "Account doesn't exists";
				else if (user.password !== (await hash(data.password))) {
					error = "The password is incorrect";
				}
			} else if (data.token) {
				user = await this.findUserFromToken(data.token);
				if (!user) error = "Unvalid token";
				else {
					const tokenInfo = this.getTokenFromHash(user, await hash(data.token));
					if (
						needPerm &&
						!tokenInfo.perms.request.has(type) &&
						!tokenInfo.perms.request.has("%ALL")
					)
						error = "This token is not allowed to perform this";
				}
			} else {
				error = "Please specify a token or account name and password";
			}

			if (error) emit(type, { error });
			return error ? null : user;
		};

		switch (type) {
			case "register": {
				const user = this.database.accounts.find((user) => user.id === data.id);
				if (user) {
					return void emit(type, {
						error: "An account with this username already exists",
					});
				}

				// Register user
				log(`Registering new account: ${data.id}`);
				await this.database.registerAccount(data.id, data.password);
				await this.updateDatabase();

				emit(type, {
					registered: true,
				});

				break;
			}

			case "generateToken": {
				const user = await handleAccess();
				if (!user) return;

				if (!data.tokenName)
					return void emit(type, {
						error: "A token name must be specified",
					});

				if (user.tokens.has(data.tokenName))
					return void emit(type, {
						error: "A token with the same name already exists",
					});

				log(
					`${socket.remoteAddress} is generating a token named "${data.tokenName}" on the "${user.id}" account.`,
				);

				const token = await user.generateToken(
					data.tokenName,
					data.perms || {},
				);
				await this.updateDatabase();

				emit(type, {
					token,
				});

				break;
			}

			case "renameAccount": {
				const user = await handleAccess();
				if (!user) return;

				if (!data.newId)
					return void emit(type, {
						error: "A new id must be specified",
					});

				if (typeof data.newId !== "string")
					return void emit(type, {
						error: "The new id must be a string",
					});

				if (this.getAccount(data.newId))
					return void emit(type, {
						error: "An account with this id already exists",
					});

				log(
					`${socket.remoteAddress} is renaming the "${user.id}" account to "${data.newId}".`,
				);

				user.id = data.newId;
				await this.updateDatabase();

				emit(type, {
					renamed: true,
				});

				break;
			}

			case "getUID": {
				const user = await handleAccess();
				if (!user) return;

				emit(type, {
					uid: user.uid,
				});

				break;
			}

			case "changePassword": {
				const user = await handleAccess();
				if (!user) return;

				if (!data.newPassword)
					return void emit(type, {
						error: "A new password must be specified",
					});

				if (typeof data.newPassword !== "string")
					return void emit(type, {
						error: "The new password must be a string",
					});

				log(
					`${socket.remoteAddress} is changing the "${user.id}" account password.`,
				);

				user.password = await hash(data.newPassword);
				await this.updateDatabase();

				emit(type, {
					changed: true,
				});
				break;
			}

			case "changePerms": {
				const user = await handleAccess();
				if (!user) return;

				if (!data.tokenName)
					return void emit(type, {
						error: "A token name must be specified",
					});

				if (!data.perms)
					return void emit(type, {
						error: "A perms object must be specified",
					});

				const token = user.tokens.has(data.tokenName);
				if (!token)
					return void emit(type, {
						error: "This token doesn't exists",
					});

				token.changePerms(data.perms);
				await this.updateDatabase();

				log(
					`${socket.remoteAddress} is changing perms of the token named "${data.tokenName}" on the "${user.id}" account.`,
				);

				emit(type, {
					changed: true,
				});
				break;
			}

			case "tokenCount": {
				const user = await handleAccess();
				if (!user) return;

				emit(type, {
					count: user.tokens.size(),
				});
				break;
			}

			case "keyCount": {
				const user = await handleAccess();
				if (!user) return;

				emit(type, {
					count: user.data.size(),
				});
				break;
			}

			case "getTokens": {
				const user = await handleAccess();
				if (!user) return;

				const tokens = structuredClone(
					Object.fromEntries(user.tokens.entries()),
				);
				for (const key of Object.keys(tokens)) {
					const token = tokens[key];
					for (const [permKey, permSet] of Object.entries(token.perms)) {
						token.perms[permKey] = Array.from(permSet);
					}
				}

				emit(type, {
					tokens,
				});
				break;
			}

			case "writeKeys": {
				const user = await handleAccess();
				if (!user) return;

				if (!data.keys)
					return void emit(type, {
						error: "A key object must be specified",
					});

				log(
					`${socket.remoteAddress} is writing keys on the "${user.id}" account.`,
				);

				let tokenInfo;
				if (data.token) {
					tokenInfo = this.getTokenFromHash(user, await hash(data.token));
				}

				for (const [key, value] of Object.entries(data.keys)) {
					if (
						!data.token ||
						tokenInfo.perms.write.has(key) ||
						tokenInfo.perms.write.has("%ALL")
					)
						await writeKey(user, key, value);
					else
						emit(type, {
							error: `This token is not allowed to write the key "${key}"`,
						});
				}
				await this.updateDatabase();

				emit(type, {
					writed: true,
				});
				break;
			}

			case "readKeys": {
				const user = await handleAccess();
				if (!user) return;

				const tokenInfo = this.getTokenFromHash(user, await hash(data.token));

				const keys = {};
				for (const key of data.keys) {
					if (
						!data.token ||
						tokenInfo.perms.read.has(key) ||
						tokenInfo.perms.read.has("%ALL")
					) {
						keys[key] = await user.readKey(key);
					} else
						emit(type, {
							error: `This token is not allowed to read the key "${key}"`,
						});
				}

				emit(type, { keys });
				break;
			}

			case "getKeys": {
				const user = await handleAccess();
				if (!user) return;

				const tokenInfo = this.getTokenFromHash(user, await hash(data.token));

				const keys = Object.fromEntries(
					Array.from(user.data).filter(
						([key, _]) =>
							tokenInfo.perms.read.has(key) || tokenInfo.perms.read.has("%ALL"),
					),
				);
				console.log(keys);

				emit(type, { keys });
				break;
			}

			case "check": {
				const user = await handleAccess(false);
				if (!user) return;

				emit(type, {
					ok: true,
				});

				break;
			}

			case "unregister": {
				const user = await handleAccess();
				if (!user) return;

				this.database.unregisterAccount(user);
				await this.updateDatabase();

				emit(type, {
					uneregistered: true,
				});

				break;
			}

			case "removeToken": {
				const user = await handleAccess();
				if (!user) return;

				if (!data.tokenName)
					return void emit(type, {
						error: "A token name must be specified",
					});

				if (!user.tokens.has(data.tokenName))
					return void emit(type, {
						error: "This token doesn't exists",
					});

				log(
					`${socket.remoteAddress} is removing the token named "${data.tokenName}" on the "${user.id}" account.`,
				);

				user.removeToken(data.tokenName);
				await this.updateDatabase();

				emit(type, {
					removed: true,
				});

				break;
			}

			default:
				break;
		}
	}

	async main(argv) {
		super.main(argv);

		// Start P3
		if (!p3.connected) await p3.connect();

		// Config database
		if (!(await FS.exists(DATABASE_PATH)) || (await FS.isEmpty(DATABASE_PATH)))
			await FS.writestr(
				DATABASE_PATH,
				JSON.stringify(new AccountDataBase().toObject()),
			);
		this.database = await AccountDataBase.from(
			JSON.parse(await FS.readstr(DATABASE_PATH)),
		);

		// Server setup
		log("Auth server: Starting...");
		this.server = p3.createServer(PORT, (socket) => {
			socket.on("message", (msg) => {
				this.handleReq(msg, socket);
			});

			// socket.on("disconnect", () => {});
		});
		log(`Auth server: Started on port ${PORT}.`);

		// Publish the server with these flags
		await p3.publish(PORT, FLAGS);
		log(`Auth server: Published with following flags: ${FLAGS.join(", ")}.`);

		await new Promise((res) => {
			this.resolve = res;
		});
	}

	ontermination() {
		log("Auth server: Stopped.");
		this.server.close();
		this.resolve();
	}
}
