class Account {
	constructor(id, password, data = {}, tokens = {}, uid = crypto.randomUUID()) {
		// rome-ignore lint/correctness/noConstructorReturn: Hashing is asyncronous
		return Promise.all([hash(password)]).then(([hashedPassword]) => {
			this.uid = uid;
			this.id = id;
			this.password = hashedPassword;
			this.data = new Map(Object.entries(data));
			this.tokens = new Map(Object.entries(tokens));

			return this;
		});
	}

	static async from(obj) {
		const { id, password, uid } = obj;
		const data = Object.fromEntries(obj.data);
		const tokens = Object.fromEntries(
			obj.tokens.map(([k, v]) => [k, Token.from(v)]),
		);

		const acc = await new this(id, "", data, tokens, uid);
		acc.password = password;
		return acc;
	}

	toObject() {
		const { id, password, uid } = this;
		const data = Array.from(this.data.entries());
		const tokens = Array.from(this.tokens.entries()).map(([k, v]) => [
			k,
			v.toObject(),
		]);

		return {
			id,
			uid,
			password,
			data,
			tokens,
		};
	}

	async writeKey(key, value) {
		if (value === null) this.data.delete(key);
		else this.data.set(key, value);
	}

	async readKey(key) {
		return this.data.get(key) || null;
	}

	async generateToken(name, perms) {
		const token = new Token(perms);
		const uuid = await token.generate();

		if (this.tokens.has(name))
			throw new Error("A token with the same name already exists");
		this.tokens.set(name, token);

		return uuid;
	}

	removeToken(name) {
		if (!this.tokens.has(name))
			throw new Error("No token found with this name");

		this.tokens.delete(name);
	}
}

// IntelliSense
if (false) module.exports = { Account };
