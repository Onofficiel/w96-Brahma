async function log(text) {
  // Making sure the file exists
  void (!(await FS.exists(LOGS_PATH)) && (await FS.touch(LOGS_PATH)));

  // Format the log message
  const date = new Date().toLocaleString("fr");
  const formatted = `[${date}]: ${text}`;

  // Print the log message
  term.println(formatted);

  // Append the log message
  const logContent = await FS.readstr(LOGS_PATH);
  await FS.writestr(
    LOGS_PATH,
    logContent ? `${logContent}\n${formatted}` : formatted
  );
}

const { simpleDigest, buffer2hex } = w96.sec.cprovider;
/**
 * Hash a string and return it as a hexadecimal string
 *
 * @param {string} text The string to hash
 * @returns The hashed hex string
 */
async function hash(text) {
  const hashBuffer = await simpleDigest(text, "SHA-512");
  const hashHex = buffer2hex(hashBuffer);

  return hashHex;
}