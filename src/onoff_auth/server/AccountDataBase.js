class AccountDataBase {
  accounts;

  toObject() {
    const accounts = this.accounts.map((acc) => acc.toObject());

    return {
      accounts,
    };
  }

  static async from(obj) {
    const accounts = await Promise.all(obj.accounts?.map(async acc => await Account.from(acc)))
    return new this(accounts);
  }

  constructor(accounts = []) {
    this.accounts = accounts;
  }

  async registerAccount(id, password, data = {}) {
    if (this.accounts.find((acc) => acc.id === id))
      throw new Error("An account with the same ID already exists");

    const acc = await new Account(id, password, data);
    this.accounts.push(acc);
  }

  unregisterAccount(account) {
    if (!(account instanceof Account))
      throw new Error("The argument must be an Account");

    this.accounts.splice(this.accounts.indexOf(account), 1);
  }
}
